﻿
namespace StrazhMonitoring.Domain.Abstraction
{
    public interface IAuthentication
    {
        int Login(string sessionId, string login, string ipAddress);

        void Logout(string sessionId);
    }
}
