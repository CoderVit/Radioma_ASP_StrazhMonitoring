﻿using StrazhMonitoring.Domain.Abstraction;
using StrazhMonitoring.Infrastructure.Context;
using System.Data.Entity.Core.Objects;

namespace StrazhMonitoring.Infrastructure.Concrete
{
    public class Authentication : IAuthentication
    {
        private readonly RDDBContext m_context = new RDDBContext();

        public int Login(string sessionId, string login, string ipAddress)
        {
            ObjectParameter objectParameter = new ObjectParameter("loginStatus", typeof(int));
            m_context.p_WSLogin(sessionId, login, ipAddress, objectParameter);
            return (int)objectParameter.Value;
        }

        public void Logout(string sessionId)
        {
            m_context.p_WSLogout(sessionId);
        }
    }
}
