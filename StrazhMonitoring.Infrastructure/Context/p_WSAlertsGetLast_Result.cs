//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace StrazhMonitoring.Infrastructure.Context
{
    using System;
    
    public partial class p_WSAlertsGetLast_Result
    {
        public int c_WUAId { get; set; }
        public int c_GPOId { get; set; }
        public System.DateTime c_CRDTime { get; set; }
        public Nullable<System.DateTime> c_CRDReceptionTime { get; set; }
        public string c_CRDExtState { get; set; }
        public string c_CRDNavStatus { get; set; }
    }
}
