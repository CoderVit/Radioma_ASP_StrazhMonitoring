﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="StrazhMonitoring.Default" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta http-equiv="content-language" content="ru" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <meta http-equiv="Expires" content="-1" />
    <title>Мониторинг</title>
    <link rel="shortcut icon" href="content/images/icons/defaultPageIcon.ico" />
    <link rel="stylesheet" type="text/css" href="content/styles/site.min.css" />
    <link rel="stylesheet" type="text/css" href="content/styles/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="content/styles/bootstrap-datepicker.min.css" />
    <link rel="stylesheet" type="text/css" href="content/styles/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" type="text/css" href="content/styles/bootstrap-toggle.min.css" />
    <link rel="stylesheet" type="text/css" href="content/styles/bootstrap-select.min.css" />
    <script type="text/javascript" src="http://api-maps.yandex.ru/2.1/?lang=ru-RU"></script>
    <script type="text/javascript" src="content/scripts/JQuery/jquery-2.1.3.min.js"></script>
    <script type="text/javascript" src="content/scripts/scripts.min.js"></script>
    <script type="text/javascript" src="content/scripts/Bootstrap/bootstrap-select.min.js"></script>
    <script type="text/javascript" src="content/scripts/Bootstrap/bootstrap-toggle.min.js"></script>
    <script type="text/javascript" src="content/Scripts/Moment/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="content/scripts/Bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="content/scripts/Bootstrap/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="content/scripts/Bootstrap/bootstrap-datepicker.ru.min.js"></script>
    <script type="text/javascript" src="content/scripts/Bootstrap/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="content/Scripts/Bootstrap/bootstrap-datetimepicker.ru.min.js"></script>
    <script type="text/javascript" src="content/Scripts/Knockout/knockout-3.3.0.min.js"></script>
    <script type="text/javascript" src="content/Scripts/Monitoring/Base/listBase.min.js"></script>
    <script type="text/javascript" src="content/Scripts/Monitoring/Base/searchBase.min.js"></script>
    <script type="text/javascript" src="content/Scripts/Monitoring/Objects/settings.min.js"></script>
    <script type="text/javascript" src="content/Scripts/Monitoring/Objects/history.min.js"></script>
    <script type="text/javascript" src="content/Scripts/Monitoring/Users/history.min.js"></script>
</head>

<body id="siteBody">
    <div id="sliderWrap">
        <div id="slider">
            <div id="sliderContent">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-3">
                            <label>Звук</label>
                        </div>
                        <div class="col-sm-3">
                            <input id="audTglBtn" type="checkbox" data-size="mini" />
                        </div>
                        <% if (isAdmin)
                           { %>
                        <div class="col-sm-3">
                            <label>События</label>
                        </div>
                        <div class="col-sm-3">
                            <input id="audTglBtnSess" type="checkbox" data-size="mini" />
                        </div>
                        <% } %>
                    </div>
                </div>
            </div>
            <div id="openCloseWrap">
                <a id="topMenuImage" href="#">
                    <img src="content/images/open.png" alt="image" />
                </a>
            </div>
        </div>
    </div>
    <div id="wrapper">
        <div id="monPanel">
            <div id="tabs">
                <ul id="tabsHdrs" class="nav nav-tabs">
                    <li class="active">
                        <a href="#objSect">Объекты</a>
                    </li>
                    <li class="dropdown">
                        <a href="#objHistSect">История</a>
                    </li>
                    <li class="dropdown">
                        <a href="#usHistSect">История визитов</a>
                    </li>
                    <li class="dropdown">
                        <a href="#settSect">Настройки</a>
                    </li>
                    <% if (isAdmin)
                       { %>
                    <li class="dropdown">
                        <a href="#usSect">Пользователи</a>
                    </li>
                    <%}%>
                </ul>
                <div id="tabsCont">
                    <div id="objSect" class="tabCont active">
                        <div id="mainTbl" class="mainTbl" runat="server">
                            <div class="mainTblHead">
                                <div class="mainTblCell">
                                    <span>ID</span>
                                </div>
                                <div class="mainTblCell">
                                    <span>Изделие</span>
                                </div>
                                <div class="mainTblCell">
                                    V <span style="font-size: 10px">км/ч</span>
                                </div>
                                <div class="mainTblCell">
                                    T <span style="font-size: 10px">посл.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="objHistSect" class="tabCont">
                        <div class="condTbl">
                            <div class="condTblHd">
                                <label>Объект: </label>
                                <select id="selIds" data-bind="selectpicker: objSel, optionsCaption: 'Выберите объект', options: selectDt"></select>
                            </div>
                            <div class="condTblR">
                                <div class="condTblC">
                                    Дата&nbsp;/&nbsp;время начала
                                </div>
                                <div class="condTblC">
                                    Дата&nbsp;/&nbsp;время конца
                                </div>
                            </div>
                            <div class="condTblR">
                                <div class="condTblC">
                                    <div class="col-sm-6" style="width: 100%;">
                                        <div class="form-group">
                                            <div class="input-group date" data-bind="datetimepicker: stTm">
                                                <input type="text" class="form-control" readonly="readonly" />
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="condTblC">
                                    <div class="col-sm-6" style="width: 100%;">
                                        <div class="form-group">
                                            <div class="input-group date" data-bind="datetimepicker: endTm">
                                                <input type="text" class="form-control" readonly="readonly" />
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="condTblC topSt">
                                    <input type="button" class="btn btn-primary" value="История" data-bind="enable: objSel() !== '', click: getData" />
                                </div>
                            </div>
                        </div>
                        <div id="objHistoryTbl" class="dtTbl">
                            <div class="dtTblHead">
                                <div class="dtTblHeadCol dtSmlCol">
                                    № п/п
                                </div>
                                <div class="dtTblHeadCol dtBigColumn">
                                    Время
                                </div>
                                <div class="dtTblHeadCol">
                                    Скорость (км/ч)
                                </div>
                                <div class="dtTblHeadCol">
                                    Канал связи
                                </div>
                                <div class="dtTblHeadCol">
                                    Уведомление
                                </div>
                                <div class="dtTblHeadCol">
                                    Тревога
                                </div>
                            </div>
                            <div id="histDt" class="dtRwGr" data-bind="foreach: objs">
                                <div data-bind="attr: { class: $parent.rowLight($data.valid, $data.caseStatement, $data.accelerometer), 'data-id': $data.index }, click: $parent.objHistRowSel.bind($parent)">
                                    <div class="dtC" data-bind="text: $data.index"></div>
                                    <div class="dtC" data-bind="text: $data.activeTime"></div>
                                    <div class="dtC" data-bind="text: $data.speed"></div>
                                    <div class="dtC" data-bind="text: $data.communicationType"></div>
                                    <div class="dtC" data-bind="text: $data.accelerometer"></div>
                                    <div class="dtC" data-bind="text: $data.caseStatement"></div>
                                </div>
                            </div>
                        </div>
                        <span data-bind="text: errMsg" class="errSpan"></span>
                    </div>
                    <div id="usHistSect" class="tabCont">
                        <div class="condTbl">
                            <div class="condTblHd">
                                Пользователь
                            <select id="usSel" data-bind="selectpicker: objSel, options: selectDt"></select>
                            </div>
                            <div class="condTblR">
                                <div class="condTblC">
                                    Дата начала
                                </div>
                                <div class="condTblC">
                                    Дата конца
                                </div>
                            </div>
                            <div class="condTblR">
                                <div class="condTblC">
                                    <div class="col-md-5" style="width: 100%;">
                                        <div class="input-group date" data-bind="datepicker: stTm">
                                            <input id="startUsrHistDate" type="text" class="form-control" readonly="readonly" />
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-th"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="condTblC">
                                    <div class="col-md-5" style="width: 100%;">
                                        <div class="input-group date" data-bind="datepicker: endTm">
                                            <input id="endUsrHistDate" type="text" class="form-control" readonly="re" />
                                            <span class="input-group-addon">
                                                <i class="glyphicon glyphicon-th"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="condTblC topSt">
                                    <input id="getUserHistory" type="button" value="История" class="btn btn-primary" data-bind="click: getData" />
                                </div>
                            </div>
                        </div>
                        <div id="usrHist" class="dtTbl mrgTp">
                            <div class="dtTblHead">
                                <div class="dtTblHeadCol">
                                    <label>Пользователь</label>
                                </div>
                                <div class="dtTblHeadCol">
                                    <label>Начало сессии</label>
                                </div>
                                <div class="dtTblHeadCol">
                                    <label>Конец сессии</label>
                                </div>
                            </div>
                            <div id="usHistDt" class="dtRwGr" data-bind="foreach: objs">
                                <div class="dtR">
                                    <div class="dtC" data-bind="text: $data.userLogin"></div>
                                    <div class="dtC" data-bind="text: $data.startVisitTime"></div>
                                    <div class="dtC" data-bind="text: $data.endVisitTime"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="settSect" class="tabCont">
                        <div class="dtTbl">
                            <div class="dtTblHead">
                                <div class="dtTblHeadCol">
                                    <label>ID</label>
                                </div>
                                <div class="dtTblHeadCol">
                                    <label>Версия прошивки</label>
                                </div>
                                <div class="dtTblHeadCol">
                                    <label>Значение интервала</label>
                                </div>
                            </div>
                            <div id="settingsDt" class="dtRwGr" data-bind="foreach: objs">
                                <div class="dtR">
                                    <div class="dtC leftAlign">
                                        <span class="blBold" data-bind="text: deviceId"></span>
                                    </div>
                                    <div class="dtC cntrAlign">
                                        <span data-bind="text: programVersion"></span>
                                    </div>
                                    <div class="dtC rightAlign">
                                        <span data-bind="text: surveyTime"></span>
                                    </div>
                                </div>
                                <div class="hiddenInfo">
                                    <div class="addTbl">
                                        <div class="addTblR">
                                            <div class="addTblC">Версия ПО</div>
                                            <div class="addTblC">
                                                <span data-bind="text: programVersion"></span>
                                            </div>
                                        </div>
                                        <div class="addTblRCl" data-bind="attr: { id: deviceId + '_tm', title: $parent.titleSet($data.isIntervalChaged, $data.isErrorApplyed, $data.surveyTime, $data.surveyTimesSet, $data.lastOperationTimeChanged) }, style: { background: $parent.rwBack($data.isIntervalChaged, $data.isErrorApplyed) }">
                                            <div class="addTblC">Интервал</div>
                                            <div class="addTblC">
                                                <span data-bind="text: surveyTimesSet"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <span data-bind="text: errMsg" class="errSpan"></span>
                    </div>
                    <% if (isAdmin)
                       {
                    %>
                    <div id="usSect" class="tabCont">
                        <div id="usCont">
                            <!-- ko if: usrs.length === 0 -->
                            <div class="dtTbl">
                                <div class="dtTblHead">
                                    <div class="dtTblHeadCol">
                                        <label>Логин</label>
                                    </div>
                                    <div class="dtTblHeadCol">
                                        <label>Администрирование</label>
                                    </div>
                                </div>
                                <div class="dtRwGr" data-bind="foreach: usrs">
                                    <div class="dtR" data-bind="event: { contextmenu: function () { $parent.usrSel($data) } }, css: { isLocked: $data.isLockedOut }">
                                        <div class="dtC leftAlign" data-bind="text: $data.userName"></div>
                                        <div class="dtC rightAlign">
                                            <button type="button" title="Объекты пользователя" class="btn btn-default" data-bind="click: $parent.getObjs.bind($parent)">
                                                <span class="glyphicon glyphicon-map-marker"></span>&nbsp;Объекты
                                            </button>
                                            <button type="button" title="Оповещения пользователя" class="btn btn-default" data-bind="click: $parent.getAlrt.bind($parent)">
                                                <span class="glyphicon glyphicon-modal-window"></span>&nbsp;Оповещения
                                            </button>
                                            <button type="button" title="Удалить пользователя" class="btn btn-default" data-bind="click: $parent.remUsr.bind($parent)">
                                                <span class="glyphicon glyphicon-remove-sign"></span>&nbsp;Удалить
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /ko -->
                        </div>
                        <div id="nwUsr">
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <button id="addNew" type="button" class="btn btn-default" data-bind="css: { disabled: isnuPanShow }, click: function () { this.isnuPanShow(true); }">
                                        <span class="glyphicon glyphicon-plus"></span>&nbsp;Создать нового пользователя
                                    </button>
                                </div>
                            </div>
                            <div id="newUserForm" class="row" data-bind="nwUsrPnl: isnuPanShow">
                                <div class="col-xs-5">
                                    <input type="text" class="form-control" placeholder="Логин" data-bind="value: newUser.login, valueUpdate: 'afterkeydown'" />
                                </div>
                                <div class="col-xs-5">
                                    <input type="password" class="form-control" placeholder="Пароль" data-bind="value: newUser.password, valueUpdate: 'afterkeydown'" />
                                </div>
                                <div class="col-xs-1">
                                    <button type="button" class="btn btn-default" data-bind="click: addUsr, css: { disabled: newUser.login().length === 0 || newUser.password().length === 0 }">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </button>
                                </div>
                                <div class="col-xs-1">
                                    <button type="button" class="btn btn-default" data-bind="click: clnNwUsr">
                                        <span class="glyphicon glyphicon-remove"></span>
                                    </button>
                                </div>
                                <span class="label label-danger" data-bind="text: errMsg"></span>
                            </div>
                        </div>
                        <div id="usObjCovDiv" class="covDiv">
                            <div id="pnlCont" class="container-fluid" tabindex="0" data-bind="event: { keyup: objsMan.keyHandl.bind(objsMan), valueupdate: 'afterkeydown' }">
                                <div id="pnlHdr" class="row">
                                    <h2>
                                        <span class="label label-info pull-left" data-bind="text: objsMan.titl"></span>
                                        <span class="pull-right" data-bind="text: objsMan.selLgn"></span>
                                    </h2>
                                </div>
                                <div id="pnlBd" class="row">
                                    <div id="objCover" data-bind="visible: objsMan.isAll"></div>
                                    <!-- ko if: objsMan.objs().length !== 0 -->
                                    <div id="objsCnt" data-bind="foreach: { data: objsMan.objs, as: 'obj' }">
                                        <span class="objItm" data-bind="text: obj.id, attr: { class: $parent.objsMan.objClass($data) }, click: $parent.objsMan.tooggled.bind($parent.objsMan)"></span>
                                    </div>
                                    <!-- /ko -->
                                    <!-- ko if: objsMan.objs().length === 0 -->
                                    <h3>Объекты отсутствуют</h3>
                                    <!-- /ko -->
                                </div>
                                <div id="pnlFtr" class="row">
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <input type="checkbox" class="checkbox" data-bind="checked: objsMan.isAll" />
                                            </div>
                                            <div class="col-md-10">
                                                <span>Выбрать все объекты (включая новые)</span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <input type="text" class="form-control" data-bind="value: objsMan.calcInpVal, enable: !objsMan.isAll()" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="btn-group pull-right">
                                            <button type="button" class="btn btn-primary" data-bind="click: objsMan.saveData.bind(objsMan)">Сохранить</button>
                                            <button type="button" class="btn btn-default" data-bind="click: objsMan.panelClean.bind(objsMan)">Отмена</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="usContMenu" class="contMenu">
                            <ul>
                                <li>x
                                    <a href="#" data-bind="click: function () { this.isnuPanShow(true); }">
                                        <i class="glyphicon glyphicon-plus"></i>&emsp;Создать
                                    </a>
                                </li>
                                <li>
                                    <a href="#" data-bind="click: mnUnlkUsr">
                                        <i class="glyphicon glyphicon-king"></i>&emsp;<span data-bind="text: !usrSel.isLockedOut ? 'Заблокировать' : 'Разблокировать'"></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" data-bind="click: function () { this.isChUsPassShow(true) }">
                                        <i class="glyphicon glyphicon-tag"></i>&emsp;Сменить пароль
                                    </a>
                                </li>
                                <li>
                                    <a id="" href="#" data-bind="click: remUsr">
                                        <i class="glyphicon glyphicon-remove"></i>&emsp;Удалить
                                    </a>
                                </li>
                                <li>
                                    <a id="" href="#" data-bind="click: getObjs">
                                        <i class="glyphicon glyphicon-map-marker"></i>&emsp;Объекты
                                    </a>
                                </li>
                                <li>
                                    <a id="" href="#" data-bind="click: getAlrt">
                                        <i class="glyphicon glyphicon-modal-window"></i>&emsp;Оповещения
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div id="chPassCover" data-bind="chUsrPassPnl: isChUsPassShow">
                            <div id="chPassCont" class="container-fluid">
                                <div id="chPassHd" class="row">
                                    <div class="col-sm-12">
                                        <label class="label label-info pull-left">Смена пароля</label>
                                        <span class="pull-right" data-bind="text: usrSel">Смена пароля</span>
                                    </div>
                                </div>
                                <div id="chPassBd" class="row">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" />
                                    </div>
                                </div>
                                <div id="chPassFtr" class="row">
                                    <div class="col-sm-12">
                                        <div class="btn-group pull-right">
                                            <button type="button" class="btn btn-sm btn-info" data-bind="">Применить</button>
                                            <button type="button" class="btn btn-sm btn-default" data-bind="click: function () { this.isChUsPassShow(false) }">Отмена</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <script type="text/javascript" src="content/Scripts/Monitoring/Users/Administration/usersManage.min.js"></script>
                    <script type="text/javascript" src="content/Scripts/Monitoring/Users/Administration/operateToObjects.min.js"></script>
                    <script type="text/javascript" src="content/Scripts/Monitoring/Users/Administration/ObjItm.min.js"></script>
                    <% } %>
                </div>
            </div>
            <div id="tbsMn">
                <div class="lstWr">
                    <ul id="listIds" runat="server">
                    </ul>
                </div>
            </div>
            <div id="contentFooter"></div>
            <a href="#" class="mp-toggle"></a>
        </div>
        <div id="mapCont">
            <div id="mpContMenu" class="contMenu">
                <ul>
                    <li>
                        <a id="cmPlShObj" href="#">
                            <i class="glyphicon glyphicon-list-alt"></i>&emsp;Показать
                        </a>
                    </li>
                    <li>
                        <a id="cmPlObjHist" href="#">
                            <i class="glyphicon glyphicon-list-alt"></i>&emsp;История
                        </a>
                    </li>

                </ul>
            </div>
        </div>
        <div id="objContMenu" class="contMenu">
            <ul>
                <li>
                    <a id="cmObjHist" href="#">
                        <i class="glyphicon glyphicon-list-alt"></i>&emsp;История
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div id="tModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <h3>Ввод нового интервала времени</h3>
                    </div>
                    <div class="row-fluid">
                        <strong>Установка времени опроса устройства №<span id="devNumb"></span></strong>
                        <span>(текущее значение - <span id="curDevVal"></span>&nbsp;минут(-ы))</span>
                    </div>
                    <div class="form-inline">
                        <input id="newDevVal" type="text" class="form-control" />
                        <strong>, минут(-ы)</strong>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="settCnf" type="button" class="btn btn-primary">Применить</button>
                </div>
            </div>
        </div>
    </div>
    <form runat="server" id="sampleForm">
        <asp:ScriptManager ID='ScriptManager1' runat='server' EnablePageMethods='true' />
    </form>
    <div id="coverDiv" class="covDiv">
        <img id="waitImg" src="/content/images/clock.gif" />
        <div id="snglConf">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12">Тревожное событие</div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <span id="alrtTxt" data-id=""></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <input id="snglCB" type="button" value="Подтвердить" class="btn btn-primary pull-right" />
                    </div>
                </div>
            </div>
        </div>
        <div id="evTblDiv" class="container-fluid">
            <div id="evTblHdr" class="row">
                <div class="col-xs-12">Тревожные события</div>
            </div>
            <div id="evTblBd" class="row">
                <div class="col-xs-12">
                    <table id="evTbl" class="tbls">
                        <tr>
                            <th>
                                <input id="selAllChB" type="checkbox" class="form-control" />
                            </th>
                            <th>Время события</th>
                            <th>Id устройства</th>
                            <th>Состояние троса</th>
                        </tr>
                    </table>
                </div>
            </div>
            <div id="evTblFtr" class="row">
                <div class="col-xs-12">
                    <input id="cnfBtn" type="button" value="Подтвердить" class="btn btn-primary pull-right" disabled="disabled" />
                </div>
            </div>
        </div>
    </div>
    <audio id="audElem" src="../content/audio/attention.mp3"></audio>
    <noscript>
        <div class="noScript">
            <h3>Для работы с сайтом, необходимо включить javascript в настроках браузера
                <a href="/Login.aspx?logout=true">Выйти</a>
            </h3>
        </div>
    </noscript>
    <script type="text/javascript" src="content/scripts/autoImplement.min.js"></script>
</body>
</html>

