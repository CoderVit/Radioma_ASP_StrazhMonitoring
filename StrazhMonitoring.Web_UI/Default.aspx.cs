﻿using StrazhMonitoring.Infrastructure.Context;
using StrazhMonitoring.Instances.GpoInstances;
using StrazhMonitoring.Instances.PersonalSettings;
using StrazhMonitoring.Instances.ServicesInstances;
using StrazhMonitoring.Services;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace StrazhMonitoring
{
    public partial class Default : Page
    {
        protected bool isAdmin;

        protected void Page_Load(object sender, EventArgs e)
        {
            SessionCheck();
            BuildBackEndData();
        }

        private void BuildBackEndData()
        {
            PendedData pendedResults;
            UserSettings settings;
            using (var rddbContext = new RDDBContext())
            {
                using (var userSettingService = new UserSettingService())
                {
                    using (var pendingService = new PendingService())
                    {
                        rddbContext.p_WSResetBuffers(Session.SessionID);
                        pendedResults = pendingService.GetPendedResults();
                        settings = userSettingService.GetSettings();
                    }
                }
            }
            if (pendedResults == null || !pendedResults.PendedResults.Any())
                Response.Redirect("Login.aspx?logout=true");
            isAdmin = settings.IsUserAdmin;
            var child = new LiteralControl
            {
                Text = BuildObjectsPanels(pendedResults.PendedResults.ToList())
            };
            mainTbl.Controls.Add(child);
            var javaScriptSerializer = new JavaScriptSerializer();
            ClientScript.RegisterStartupScript(GetType(), "startAction",
                string.Concat("stAct(", javaScriptSerializer.Serialize(pendedResults), ", ",
                    javaScriptSerializer.Serialize(settings), ")"), true);
        }

        private string BuildObjectsPanels(List<GpoDetail> gpoDetail)
        {
            var resultString = string.Empty;
            gpoDetail.ForEach(item =>
            {
                var text = item.Id.ToString();
                var htmlGenericControl = new HtmlGenericControl("li");
                htmlGenericControl.Attributes.Add("data-id", text);
                htmlGenericControl.Attributes.Add("title", text);
                var htmlGenericControl2 = new HtmlGenericControl
                {
                    InnerHtml = text.Length <= 7 ? text : "~ " + text.Substring(text.Length - 7),
                    TagName = "a"
                };
                htmlGenericControl2.Attributes.Add("href", "#");
                htmlGenericControl.Controls.Add(htmlGenericControl2);
                listIds.Controls.Add(htmlGenericControl);
                object resultString1 = resultString;
                resultString = string.Concat(resultString1, "<div class='mainTblRow",
                    item.CaseStatement.Contains("</span>") ? " devOpen" : string.Empty, "' data-id=", text,
                    "><div class='mainTblCell'><span class=blBold>", text,
                    "</span></div><div class=mainTblCell>Изделие тип 1</div><div id=", text,
                    "_GpoSpeed class=mainTblCell>", item.Speed, "</div><div id=", text, "_ActiveTime class=mainTblCell>",
                    item.ActiveTime, "</div></div><div class=\"hiddenInfo\"><div id=\"", text,
                    "_row\" class=\"addTbl\"><div class=\"addTblR\"><div class=\"addTblC\">N Широта</div><div id=\"",
                    text, "_Latitude\" class=\"addTblC bl\">", item.Coordinates.Latitude,
                    "</div></div><div class=\"addTblR\"><div class=\"addTblC\">E Долгота</div><div id=\"", text,
                    "_Longtitude \"class=\"addTblC bl\">", item.Coordinates.Longitude,
                    "</div></div><div class=\"addTblR\"><div class=\"addTblC\">Батарея</div><div id=\"", text,
                    "_Battery\" class=\"addTblC\">", item.Battery,
                    "</div></div><div class=\"addTblR\"><div class=\"addTblC\">Валидность</div><div id=\"", text,
                    "_Valid\" class=\"addTblC\">", item.Valid,
                    "</div></div><div class=\"addTblR\"><div class=\"addTblC\">Канал</div><div id=\"", text,
                    "_CommunicationType\" class=\"addTblC\">", item.CommunicationType,
                    "</div></div><div class=\"addTblR\"><div class=\"addTblC\">Трос</div><div id=\"", text,
                    "_CaseStatement\" class=\"addTblC\">", item.CaseStatement,
                    "</div></div><div class=\"addTblR\"><div class=\"addTblC\">Акселерометр</div><div id=\"", text,
                    "_Accelerometer>\" class=\"addTblC\">", item.Accelerometer, "</div></div>");
                var flag = !string.IsNullOrEmpty(item.SealedName);
                if (!flag)
                    resultString = resultString + "<div id=" + text +
                                   "_noHaveSealedData class=addTblR><div class=addTblC>Опломбирование</div><div class=addTblC>не производилось</div></div>";
                var resultString2 = resultString;
                resultString = string.Concat(resultString2, "<div id=\"", text, "_SealedData\" class=\"sealedTblRows",
                    !flag ? " nonVisible" : "",
                    "\"><div class=\"addTblR\"><div class=\"addTblC\">Фамилия И.О.: </div><div id=\"", text,
                    "_SealedName\" class=\"addTblC\">", item.SealedName,
                    "</div></div><div class=\"addTblR\"><div class=\"addTblC\">Опломбировано дата: </div><div id=\"",
                    text, "_SealedDate\" class=\"addTblC\">", item.SealedDate,
                    "</div></div><div class=\"addTblR\"><div class=\"addTblC\">Опломбировано время: </div><div id=\"",
                    text, "_SealedTime\" class=\"addTblC\">", item.SealedTime,
                    "</div></div><div class=\"addTblR\"><div class=\"addTblC\">№ ЗПУ: </div><div id=\"", text,
                    "_SealedZpuNumber\" class=\"addTblC\">", item.SealedZpuNumber,
                    "</div></div><div class=\"addTblR\"><div class=\"addTblC\">№ вагона: </div><div id=\"", text,
                    "_SealedVagonNumber\" class=\"addTblC\">", item.SealedVagonNumber,
                    "</div></div><div class=\"addTblR\"><div class=\"addTblC\">№ контейнера: </div><div id=\"", text,
                    "_SealedContainerNumber\" class=\"addTblC\">", item.SealedContainerNumber,
                    "</div></div><div class=\"addTblR\"><div class=\"addTblC\">N Широта МРМ: </div><div id=\"", text,
                    "_SealedLongitude\" class=\"addTblC\">", item.SealedLatitude,
                    "</div></div><div class=\"addTblR\"><div class=\"addTblC\">E Долгота МРМ: </div><div id=\"", text,
                    "_SealedLatitude\" class=\"addTblC\">", item.SealedLongitude,
                    "</div></div></div><div class=clear></div></div></div>");
            });
            return resultString;
        }

        private void SessionCheck()
        {
            using (var context = new RDDBContext())
            {
                var objectParameter = new ObjectParameter("alive", typeof(bool));
                context.p_WSCheck(Session.SessionID, objectParameter);
                if (!(bool)objectParameter.Value)
                    Response.Redirect("Login.aspx?logout=true");
            }
        }
    }
}