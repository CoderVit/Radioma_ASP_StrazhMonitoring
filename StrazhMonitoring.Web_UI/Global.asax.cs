﻿using System;
using System.Web;

namespace StrazhMonitoring
{
    public class Global : System.Web.HttpApplication
    {
        protected void Session_Start(object sender, EventArgs e)
        {
            base.Session["init"] = 0;
        }

        protected void Application_BeginRequest()
        {
            HttpContext.Current.Response.Cache.SetExpires(DateTime.UtcNow.AddDays(-1.0));
            HttpContext.Current.Response.Cache.SetValidUntilExpires(false);
            HttpContext.Current.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
            HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            HttpContext.Current.Response.Cache.SetNoStore();
        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            if (!HttpContext.Current.Response.Status.StartsWith("401"))
            {
                return;
            }
            HttpContext.Current.Response.ClearContent();
            base.Response.Redirect("Login.aspx");
        }
    }
}