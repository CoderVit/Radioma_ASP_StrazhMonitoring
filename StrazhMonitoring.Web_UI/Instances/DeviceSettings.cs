﻿
namespace StrazhMonitoring.Instances
{
    public class DeviceSettings
    {
        public int DeviceId
        {
            get;
            set;
        }

        public string ProgramVersion
        {
            get;
            set;
        }

        public double SurveyTime
        {
            get;
            set;
        }

        public double SurveyTimesSet
        {
            get;
            set;
        }

        public bool? IsIntervalChaged
        {
            get;
            set;
        }

        public bool IsErrorApplyed
        {
            get;
            set;
        }

        public string LastDeviceConnection
        {
            get;
            set;
        }

        public string LastOperationTimeChanged
        {
            get;
            set;
        }
    }
}