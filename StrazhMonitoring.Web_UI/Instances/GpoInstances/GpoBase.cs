﻿
namespace StrazhMonitoring.Instances.GpoInstances
{
    public class GpoBase
    {
        public string Accelerometer
        {
            get;
            set;
        }

        public string ActiveTime
        {
            get;
            set;
        }

        public string Battery
        {
            get;
            set;
        }

        public string CaseStatement
        {
            get;
            set;
        }

        public Coordinates Coordinates
        {
            get;
            set;
        }

        public string CommunicationType
        {
            get;
            set;
        }

        public string Valid
        {
            get;
            set;
        }

        public string Speed
        {
            get;
            set;
        }
    }
}