﻿
namespace StrazhMonitoring.Instances.GpoInstances
{
    public class GpoDetail : GpoBase
    {
        public int Id
        {
            get;
            set;
        }

        public double Course
        {
            get;
            set;
        }

        public string SealedName
        {
            get;
            set;
        }

        public string SealedDate
        {
            get;
            set;
        }

        public string SealedTime
        {
            get;
            set;
        }

        public string SealedZpuNumber
        {
            get;
            set;
        }

        public string SealedVagonNumber
        {
            get;
            set;
        }

        public string SealedContainerNumber
        {
            get;
            set;
        }

        public string SealedLongitude
        {
            get;
            set;
        }

        public string SealedLatitude
        {
            get;
            set;
        }
    }
}