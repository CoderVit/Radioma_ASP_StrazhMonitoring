﻿using StrazhMonitoring.Instances.GpoInstances;

namespace StrazhMonitoring.Instances.HistoriesInstances
{
    public class GpoHistoryBaloon : GpoBase
    {
        public string AlertState
        {
            get;
            set;
        }
    }
}