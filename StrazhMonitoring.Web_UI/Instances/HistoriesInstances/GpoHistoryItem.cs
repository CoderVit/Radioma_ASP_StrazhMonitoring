﻿using StrazhMonitoring.Instances.GpoInstances;

namespace StrazhMonitoring.Instances.HistoriesInstances
{
    public class GpoHistoryItem : GpoBase
    {
        public int Index
        {
            get;
            set;
        }
    }
}