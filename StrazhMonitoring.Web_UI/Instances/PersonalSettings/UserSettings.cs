﻿
namespace StrazhMonitoring.Instances.PersonalSettings
{
    public class UserSettings
    {
        public bool IsSoundEnabled
        {
            get;
            set;
        }

        public bool IsObjectHistiryWrap
        {
            get;
            set;
        }

        public bool IsHistoryBaloonShow
        {
            get;
            set;
        }

        public bool IsUserAdmin
        {
            get;
            set;
        }

        public bool IsAlertsEnabled
        {
            get;
            set;
        }
    }
}