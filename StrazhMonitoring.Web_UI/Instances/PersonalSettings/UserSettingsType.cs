﻿
namespace StrazhMonitoring.Instances.PersonalSettings
{
    public enum UserSettingsType
    {
        USER_SETTINGS_TYPE_SOUND_NOTIFIER = 1,
        USER_SETTINGS_TYPE_WRAP_OBJECT_HISTORY,
        USER_SETTINGS_TYPE_SHOW_HISTORY_BALOON,
        USER_SETTINGS_TYPE_IS_USER_ADMIN,
        USER_SETTINGS_TYPE_IS_ALERTS_ENABLED
    }
}