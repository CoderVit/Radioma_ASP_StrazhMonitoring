﻿
namespace StrazhMonitoring.Instances.ServicesInstances
{
    public class DevicesEvent
    {
        public int EventsId
        {
            get;
            set;
        }

        public string OccuredTime
        {
            get;
            set;
        }

        public int DeviceId
        {
            get;
            set;
        }

        public string AlertStatement
        {
            get;
            set;
        }
    }
}