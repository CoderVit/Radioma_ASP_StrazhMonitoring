﻿using StrazhMonitoring.Instances.GpoInstances;
using System.Collections.Generic;

namespace StrazhMonitoring.Instances.ServicesInstances
{
    public class PendedData
    {
        public IEnumerable<GpoDetail> PendedResults
        {
            get;
            set;
        }

        public IEnumerable<DevicesEvent> DevicesEvents
        {
            get;
            set;
        }

        public PendedData()
        {
            this.PendedResults = new List<GpoDetail>();
            this.DevicesEvents = new List<DevicesEvent>();
        }
    }
}