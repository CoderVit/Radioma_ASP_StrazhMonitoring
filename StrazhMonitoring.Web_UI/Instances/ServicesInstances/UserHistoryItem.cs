﻿
namespace StrazhMonitoring.Instances.ServicesInstances
{
    public class UserHistoryItem
    {
        public string UserLogin
        {
            get;
            set;
        }

        public string StartVisitTime
        {
            get;
            set;
        }

        public string EndVisitTime
        {
            get;
            set;
        }
    }
}