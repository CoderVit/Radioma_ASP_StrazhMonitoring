﻿
namespace StrazhMonitoring.Instances
{
    public class SettingData
    {
        internal int GpodId
        {
            get;
            set;
        }

        internal long? Interval
        {
            get;
            set;
        }

        internal long? OldInterval
        {
            get;
            set;
        }

        internal string Commentary
        {
            get;
            set;
        }

        internal bool IsApplied
        {
            get;
            set;
        }
    }
}