﻿
namespace StrazhMonitoring.Instances.UserAdministration
{
    public class ObjectsData
    {
        public string ObjectsCollection
        {
            get;
            private set;
        }

        public string ObjectsBindinds
        {
            get;
            private set;
        }

        public ObjectsData(string objects, string objectsBindings)
        {
            this.ObjectsCollection = objects;
            this.ObjectsBindinds = objectsBindings;
        }
    }
}