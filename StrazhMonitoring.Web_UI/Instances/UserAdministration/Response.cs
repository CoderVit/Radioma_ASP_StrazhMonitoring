﻿
namespace StrazhMonitoring.Instances.UserAdministration
{
    public class Response
    {
        public int Code
        {
            get;
            set;
        }

        public string Message
        {
            get;
            set;
        }
    }
}