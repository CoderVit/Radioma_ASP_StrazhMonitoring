﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="StrazhMonitoring.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<link href="content/styles/auth.min.css" rel="stylesheet" />
	<title>Авторизация</title>
</head>
<body>
	<div class="container">
		<section id="content">
			<form ID="loginForm" runat="server">
				<h1>Вход в систему</h1>
				<div>
					<asp:TextBox ID="usernameTbtBx" placeholder="Логин" runat="server" CssClass="required"></asp:TextBox>
				</div>
				<div>
					<asp:TextBox ID="passwordTbtBx" TextMode="Password" placeholder="Пароль"  runat="server" CssClass="required"></asp:TextBox>
				</div>
				<div align="left" style="padding-left: 7px;">
						<label>
							<asp:CheckBox ID="PersistantChB" runat="server" />Запомнить меня
						</label>
				</div>
				<asp:Button type="submit" Text="Войти" runat="server" OnClick="SignIn"/>
				<div align="right">
					<asp:Label runat="server" ID="statusLbl" ForeColor="Red"></asp:Label>
				</div>
			</form>
		</section>
	</div>	
</body>
</html>
