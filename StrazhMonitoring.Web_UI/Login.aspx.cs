﻿using StrazhMonitoring.Domain.Abstraction;
using StrazhMonitoring.Infrastructure.Concrete;
using System;
using System.Web;
using System.Web.Security;
using StrazhMonitoring.Services;

namespace StrazhMonitoring
{
    public partial class Login : System.Web.UI.Page
    {
        private IAuthentication m_authentication;

        private string m_sessionId;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.m_sessionId = this.Session.SessionID;
            this.m_authentication = new Authentication();
            if (base.Request.QueryString.HasKeys() && base.Request.QueryString["logout"] == "true" && base.Request.IsAuthenticated)
            {
                this.SignOut();
            }
        }

        public void SignIn(object sender, EventArgs e)
        {
            string text = this.usernameTbtBx.Text;
            string text2 = this.passwordTbtBx.Text;
            if (string.IsNullOrEmpty(text) || string.IsNullOrEmpty(text2))
            {
                this.statusLbl.Text = "Заполните все поля формы";
                return;
            }
            if (!Membership.ValidateUser(text, text2))
            {
                this.statusLbl.Text = "Логин или пароль некорректны";
                return;
            }
            string ipAddress = HttpContext.Current.Request.Params["HTTP_CLIENT_IP"] ?? HttpContext.Current.Request.UserHostAddress;
            switch (this.m_authentication.Login(this.m_sessionId, text, ipAddress))
            {
                case 0:
                    this.SignIn(text);
                    return;
                case 1:
                    {
                        int num = 0;
                        do
                        {
                            switch (this.m_authentication.Login(this.m_sessionId, text, ipAddress))
                            {
                                case 0:
                                    this.SignIn(text);
                                    num = 5;
                                    break;
                                case 1:
                                    this.SignOut();
                                    num++;
                                    break;
                                default:
                                    num = 5;
                                    break;
                            }
                        }
                        while (num != 5);
                        return;
                    }
                case 2:
                    this.statusLbl.Text = "Проверьте настройки пользователя";
                    return;
                case 3:
                    this.statusLbl.Text = "Вход невозможен: отсутствуют объекты контроля";
                    return;
                case 4:
                    this.statusLbl.Text = "Пользователь временно заблокирован";
                    return;
                default:
                    this.statusLbl.Text = "Обратитесь в службу тех. поддержки";
                    return;
            }
        }

        private void SignIn(string userName)
        {
            bool isAdmin;
            using (var context = new UserSettingService())
            {
                isAdmin = context.GetSettings().IsUserAdmin;
            }
            base.Response.Cookies.Add(new HttpCookie("user", isAdmin.ToString()));
            FormsAuthentication.SetAuthCookie(userName, this.PersistantChB.Checked);
            Response.Redirect("/Default.aspx");
        }

        protected void SignOut()
        {
            if (m_sessionId == null)
            {
                return;
            }
            m_authentication.Logout(m_sessionId);
            Session.Abandon();
            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", string.Empty));
            FormsAuthentication.SignOut();
        }
    }
}