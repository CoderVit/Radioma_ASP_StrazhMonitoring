﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Web;

namespace StrazhMonitoring.Services.Base
{
    public class ServiceBase
    {
        protected const string FAILED_PARAMETERS_EXCEPTION = "missed parameter(-s)";

        protected const string GET_METHOD_TYPE = "GET";

        protected const string POST_METHOD_TYPE = "POST";

        protected readonly string sessionId = HttpContext.Current.Session.SessionID;

        protected readonly string timeFormat = "dd.MM.yyyy HH:mm:ss";

        protected string ToJson(object objectsToJson)
        {
            if (objectsToJson != null)
            {
                return JsonConvert.SerializeObject(objectsToJson, Formatting.Indented, new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                });
            }
            return "";
        }

        protected string ToBatteryField(string status, double? voltage)
        {
            string text = string.IsNullOrEmpty(status) ? string.Empty : ("<span class='rdBold'>" + status.ToUpper() + "</span>");
            object obj = text;
            return string.Concat(new object[]
            {
                obj,
                " <span class=bl>",
                voltage,
                "</span> B."
            });
        }
    }
}