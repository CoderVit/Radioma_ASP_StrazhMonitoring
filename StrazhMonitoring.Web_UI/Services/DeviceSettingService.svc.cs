﻿using StrazhMonitoring.Infrastructure.Context;
using StrazhMonitoring.Instances;
using StrazhMonitoring.Services.Base;
using System;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Web.Script.Services;

namespace StrazhMonitoring.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed), ServiceContract]
    public class DeviceSettingService : ServiceBase, IDisposable
    {
        [OperationContract, WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json), ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetDevicesSettings()
        {
            DeviceSettings[] objectsToJson;
            using (RDDBContext rDDBContext = new RDDBContext())
            {
                objectsToJson = (from item in rDDBContext.p_GPOPIGetList_intervals(this.sessionId)
                                 select new DeviceSettings
                                 {
                                     DeviceId = item.c_GPOId,
                                     ProgramVersion = item.FW,
                                     IsIntervalChaged = new bool?(Convert.ToBoolean(item.IntervalChangingFlag ?? 0)),
                                     IsErrorApplyed = Convert.ToBoolean(item.IntervalChangingError),
                                     LastDeviceConnection = (item.ValuePendingTime.HasValue ? item.ValuePendingTime.Value.ToLocalTime().ToString(this.timeFormat) : ""),
                                     LastOperationTimeChanged = (item.OperationDateTime.HasValue ? item.OperationDateTime.Value.ToLocalTime().ToString(this.timeFormat) : ""),
                                     SurveyTime = this.ToMinutes(item.Value),
                                     SurveyTimesSet = (item.ValuePending.HasValue ? this.ToMinutes(item.ValuePending.Value) : 0.0)
                                 }).ToArray<DeviceSettings>();
            }
            return base.ToJson(objectsToJson);
        }

        [OperationContract, WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public void SettingsSet(int? deviceId, int? parameterId, double? pendingData)
        {
            if (!deviceId.HasValue || !parameterId.HasValue || !pendingData.HasValue)
            {
                throw new ArgumentException("missed parameter(-s)");
            }
            using (RDDBContext rDDBContext = new RDDBContext())
            {
                rDDBContext.p_GPOPISet_intervals(deviceId, new int?(101), new int?(this.ToSeconds(pendingData.Value)));
            }
        }

        private double ToMinutes(int seconds)
        {
            return Math.Round(Convert.ToDouble((double)seconds / 60.0), 1);
        }

        private int ToSeconds(double paramsVal)
        {
            double num = Math.Round(paramsVal, 1);
            return Convert.ToInt32(num * 60.0);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
