﻿using StrazhMonitoring.Infrastructure.Context;
using StrazhMonitoring.Instances;
using StrazhMonitoring.Instances.HistoriesInstances;
using StrazhMonitoring.Services.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;

namespace StrazhMonitoring.Services
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class HistoryService : ServiceBase, IDisposable
    {
        [OperationContract, WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public string GetObjectHistory(int? id, string startTime, string endTime)
        {
            DateTime dateTime;
            DateTime dateTime2;
            if (!id.HasValue || !DateTime.TryParse(startTime, out dateTime) || !DateTime.TryParse(endTime, out dateTime2))
            {
                throw new Exception("missed parameter(-s)");
            }
            GpoHistoryItem[] objectsToJson;
            using (RDDBContext rDDBContext = new RDDBContext())
            {
                objectsToJson = rDDBContext.p_WSCoordinatesGetHistory(this.sessionId, new int?(id.Value), new DateTime?(dateTime.ToUniversalTime()), new DateTime?(dateTime2.ToUniversalTime())).Select((p_WSCoordinatesGetHistory_Result item, int index) => new GpoHistoryItem
                {
                    Index = index + 1,
                    Coordinates = new Coordinates
                    {
                        Latitude = (item.c_CRDLatitude ?? 0m),
                        Longitude = (item.c_CRDLongitude ?? 0m)
                    },
                    ActiveTime = item.c_CRDTime.ToLocalTime().ToString(this.timeFormat),
                    Speed = item.c_CRDSpeed.ToString(),
                    CaseStatement = item.case_open,
                    CommunicationType = item.communicationtype,
                    Accelerometer = item.accelerometer,
                    Valid = item.valid
                }).ToArray<GpoHistoryItem>();
            }
            return base.ToJson(objectsToJson);
        }

        [OperationContract, WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public IEnumerable<int> GetDevicesId()
        {
            IEnumerable<int> result;
            using (RDDBContext rDDBContext = new RDDBContext())
            {
                result = (from item in rDDBContext.p_WSGPOsList(this.sessionId)
                          where item.HasValue
                          select item.Value).ToList<int>();
            }
            return result;
        }

        [OperationContract, WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public string GetGpoHistoryBaloon(int? gpoId, string historyTime, string communicationType)
        {
            DateTime dateTime;
            if (!gpoId.HasValue || string.IsNullOrEmpty(communicationType) || !DateTime.TryParse(historyTime, out dateTime))
            {
                throw new Exception("missed parameter(-s)");
            }
            GpoHistoryBaloon objectsToJson;
            using (RDDBContext rDDBContext = new RDDBContext())
            {
                objectsToJson = (from item in rDDBContext.p_WSCoordinatesGetPointInfo(this.sessionId, gpoId, new DateTime?(dateTime.ToUniversalTime()), communicationType)
                                 select new GpoHistoryBaloon
                                 {
                                     ActiveTime = item.c_CRDTime.ToLocalTime().ToString(this.timeFormat),
                                     Speed = item.c_CRDSpeed.ToString(),
                                     Coordinates = new Coordinates
                                     {
                                         Longitude = (item.c_CRDLongitudePrevValue ?? 0m),
                                         Latitude = (item.c_CRDLatitudePrevValue ?? 0m)
                                     },
                                     AlertState = (item.case_open.Contains("Закрыто") ? "Отсутствует" : ("<span class='rdBold'>" + item.case_open.ToUpper() + "</span>")),
                                     Battery = base.ToBatteryField(item.batt, item.voltage),
                                     Valid = item.valid,
                                     CommunicationType = item.communicationtype,
                                     CaseStatement = item.case_open,
                                     Accelerometer = item.accelerometer
                                 }).SingleOrDefault<GpoHistoryBaloon>();
            }
            return base.ToJson(objectsToJson);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
