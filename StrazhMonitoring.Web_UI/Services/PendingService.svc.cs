﻿using StrazhMonitoring.Infrastructure.Context;
using StrazhMonitoring.Instances;
using StrazhMonitoring.Instances.GpoInstances;
using StrazhMonitoring.Instances.ServicesInstances;
using StrazhMonitoring.Services.Base;
using System;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;

namespace StrazhMonitoring.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required), ServiceContract]
    public class PendingService : ServiceBase, IDisposable
    {
        [OperationContract, WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public void BuferReset()
        {
            using (RDDBContext rDDBContext = new RDDBContext())
            {
                rDDBContext.p_WSResetBuffers(this.sessionId);
            }
        }

        [OperationContract, WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public PendedData GetPendedResults()
        {
            PendedData pendedData = new PendedData();
            using (RDDBContext rDDBContext = new RDDBContext())
            {
                ObjectParameter objectParameter = new ObjectParameter("ConnectionStatus", typeof(int));
                ObjectParameter objectParameter2 = new ObjectParameter("CoordinatesCount", typeof(int));
                ObjectParameter objectParameter3 = new ObjectParameter("AlertsCount", typeof(int));
                rDDBContext.p_WSCheckStatus(this.sessionId, objectParameter, objectParameter2, objectParameter3);
                if ((int)objectParameter.Value == -5)
                {
                    return null;
                }
                if ((int)objectParameter2.Value > 0)
                {
                    pendedData.PendedResults = rDDBContext.p_WSCoordinatesGetPended(this.sessionId).Select(delegate(p_WSCoordinatesGetPended_Result item)
                    {
                        DateTime d = (item.MRM_date == new DateTime(1900, 1, 1, 0, 0, 0)) ? DateTime.MinValue : item.MRM_date;
                        GpoDetail gpoDetail = new GpoDetail();
                        gpoDetail.Id = item.c_GPOId;
                        gpoDetail.Speed = ((item.communicationtype == "GPRS") ? item.c_CRDSpeed.ToString() : string.Empty);
                        GpoDetail arg_95_0 = gpoDetail;
                        double? c_CRDCourse = item.c_CRDCourse;
                        arg_95_0.Course = (c_CRDCourse.HasValue ? c_CRDCourse.GetValueOrDefault() : 0.0);
                        gpoDetail.ActiveTime = item.c_CRDTime.ToLocalTime().ToString("dd.MM.yyyy HH:mm:ss");
                        gpoDetail.Coordinates = new Coordinates
                        {
                            Longitude = (item.c_CRDLongitude ?? 0m),
                            Latitude = (item.c_CRDLatitude ?? 0m)
                        };
                        gpoDetail.Battery = base.ToBatteryField(item.batt, item.voltage);
                        gpoDetail.Valid = (item.valid ?? "нет");
                        gpoDetail.CommunicationType = (item.communicationtype ?? "нет");
                        gpoDetail.CaseStatement = ((item.case_open == "Вскрыто") ? ("<span class='rdBold'>" + item.case_open.ToUpper() + "</span>") : item.case_open);
                        gpoDetail.Accelerometer = item.accelerometer;
                        gpoDetail.SealedName = ((item.Surname != "нет") ? string.Concat(new string[]
						{
							item.Surname,
							" ",
							item.Name,
							" ",
							item.Patronymic
						}) : string.Empty);
                        gpoDetail.SealedDate = ((d == DateTime.MinValue) ? "нет" : item.MRM_date.ToShortDateString());
                        gpoDetail.SealedTime = ((d == DateTime.MinValue) ? "нет" : item.MRM_date.ToLongTimeString());
                        gpoDetail.SealedZpuNumber = (item.zpuNum ?? "нет");
                        gpoDetail.SealedVagonNumber = (item.vagonNum ?? "нет");
                        gpoDetail.SealedContainerNumber = (item.containerNum ?? "нет");
                        gpoDetail.SealedLongitude = (item.Lon ?? "нет");
                        gpoDetail.SealedLatitude = (item.Lat ?? "нет");
                        return gpoDetail;
                    }).ToList<GpoDetail>();
                }
                if ((int)objectParameter3.Value > 0)
                {
                    pendedData.DevicesEvents = (from item in rDDBContext.p_WSAlertsGetLast(this.sessionId, null)
                                                select new DevicesEvent
                                                {
                                                    EventsId = item.c_WUAId,
                                                    DeviceId = item.c_GPOId,
                                                    OccuredTime = item.c_CRDTime.ToLocalTime().ToString(this.timeFormat),
                                                    AlertStatement = item.c_CRDExtState
                                                }).ToList<DevicesEvent>();
                }
            }
            return pendedData;
        }

        [OperationContract, WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public void ConfirmErrorStatement(int? eventId)
        {
            if (!eventId.HasValue)
            {
                return;
            }
            using (RDDBContext rDDBContext = new RDDBContext())
            {
                rDDBContext.p_WSAlertDelivered(this.sessionId, eventId);
            }
        }

        [OperationContract, WebInvoke(Method = "POST")]
        public void ConfirmErrorsStatements(int[] readEventsId)
        {
            if (!readEventsId.Any<int>())
            {
                return;
            }
            using (RDDBContext rddbContext = new RDDBContext())
            {
                readEventsId.ToList<int>().ForEach(delegate(int eventItem)
                {
                    rddbContext.p_WSAlertDelivered(this.sessionId, new int?(eventItem));
                });
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
