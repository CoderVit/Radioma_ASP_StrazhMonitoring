﻿using StrazhMonitoring.Infrastructure.Context;
using StrazhMonitoring.Instances.UserAdministration;
using StrazhMonitoring.Services.Base;
using System;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Web.Security;

namespace StrazhMonitoring.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required), ServiceContract]
    public class UserAdministrationService : ServiceBase
    {
        private const string SUCCESS_OPERATION_MESSAGE = "Успешное выполнения операции";

        private const string FAIL_OPERATION_MESSAGE = "Ошибка выполнения операции";

        private const string UNATHORIZE_OPERATION_MESSAGE = "Неавторизированный доступ";

        [OperationContract, WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public string UserAdd(string userName, string password)
        {
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
            {
                throw new ArgumentNullException("missed parameter(-s)");
            }
            try
            {
                if (!AuthorizationCheck())
                {
                    throw new UnauthorizedAccessException();
                }
                Membership.CreateUser(userName, password);
            }
            catch (MembershipCreateUserException exception)
            {
                string result = ToJson(new Response
                {
                    Code = -1,
                    Message = "Данный логин уже существует"
                });
                return result;
            }
            catch (UnauthorizedAccessException)
            {
                string result = ToJson(new Response
                {
                    Code = -1,
                    Message = "Неавторизированный доступ"
                });
                return result;
            }
            catch (Exception)
            {
                string result = ToJson(new Response
                {
                    Code = -1,
                    Message = "Ошибка выполнения операции"
                });
                return result;
            }
            return ToJson(new Response
            {
                Code = 0,
                Message = "Успешное выполнения операции"
            });
        }

        [OperationContract, WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public string UserUpdate(string userName, string password)
        {
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
            {
                throw new ArgumentNullException("missed parameter(-s)");
            }
            try
            {
                if (!this.AuthorizationCheck())
                {
                    throw new UnauthorizedAccessException();
                }
                MembershipUser user = Membership.GetUser(userName);
                if (user == null)
                {
                    throw new ArgumentNullException("userName");
                }
                user.ChangePassword(user.ResetPassword(), password);
            }
            catch (MembershipCreateUserException)
            {
                string result = base.ToJson(new Response
                {
                    Code = -1,
                    Message = "Данный логин уже существует"
                });
                return result;
            }
            catch (ArgumentNullException)
            {
                string result = base.ToJson(new Response
                {
                    Code = -1,
                    Message = "Пользователь не найден"
                });
                return result;
            }
            return base.ToJson(new Response
            {
                Code = 0,
                Message = "Пользователь изменен"
            });
        }

        [OperationContract, WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public string UserDelete(string userName)
        {
            if (string.IsNullOrEmpty(userName))
            {
                throw new ArgumentNullException(userName);
            }
            try
            {
                if (!this.AuthorizationCheck())
                {
                    throw new UnauthorizedAccessException();
                }
                Membership.DeleteUser(userName);
            }
            catch (Exception)
            {
                return base.ToJson(new Response
                {
                    Code = -1,
                    Message = "Ошибка выполнения операции"
                });
            }
            return base.ToJson(new Response
            {
                Code = 0,
                Message = "Успешное выполнения операции"
            });
        }

        [OperationContract, WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public string UsersGet()
        {
            if (!this.AuthorizationCheck())
            {
                throw new UnauthorizedAccessException("unathorized access");
            }
            p_WURetrieve_Result[] objectsToJson;
            using (RDDBContext rDDBContext = new RDDBContext())
            {
                objectsToJson = (from item in rDDBContext.p_WURetrieve(this.sessionId)
                                 orderby item.UserName
                                 select item).ToArray<p_WURetrieve_Result>();
            }
            return base.ToJson(objectsToJson);
        }

        private bool AuthorizationCheck()
        {
            ObjectParameter objectParameter = new ObjectParameter("Alive", typeof(bool));
            using (RDDBContext rDDBContext = new RDDBContext())
            {
                rDDBContext.p_WSCheck(this.sessionId, objectParameter);
            }
            return (bool)objectParameter.Value;
        }
    }
}
