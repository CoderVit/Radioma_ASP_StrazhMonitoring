﻿using StrazhMonitoring.Infrastructure.Context;
using StrazhMonitoring.Instances.ServicesInstances;
using StrazhMonitoring.Services.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;

namespace StrazhMonitoring.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required), ServiceContract]
    public class UserHistoryService : ServiceBase, IDisposable
    {
        [OperationContract, WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public string GetLastUserHistory()
        {
            UserHistoryItem[] objectsToJson;
            using (RDDBContext rDDBContext = new RDDBContext())
            {
                string lastUserName = (from item in rDDBContext.t_WebSessions
                                       orderby item.c_WSTimeLogin descending
                                       select item).Take(1).Single<t_WebSessions>().c_WSUserName;
                DateTime lastweek = DateTime.Now.AddDays(-7.0);
                objectsToJson = (from item in rDDBContext.t_WebSessions.ToList<t_WebSessions>()
                                 where item.c_WSUserName == lastUserName && item.c_WSTimeLogin >= lastweek
                                 select item into userHistory
                                 select new UserHistoryItem
                                 {
                                     UserLogin = userHistory.c_WSUserName,
                                     StartVisitTime = userHistory.c_WSTimeLogin.ToLocalTime().ToString(this.timeFormat),
                                     EndVisitTime = (userHistory.c_WSTimeLogout.HasValue ? userHistory.c_WSTimeLogout.Value.ToLocalTime().ToString(this.timeFormat) : string.Empty)
                                 }).ToArray<UserHistoryItem>();
            }
            return base.ToJson(objectsToJson);
        }

        [OperationContract, WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public IEnumerable<string> GetUsersLogins()
        {
            IEnumerable<string> result;
            using (RDDBContext rDDBContext = new RDDBContext())
            {
                result = (from item in rDDBContext.aspnet_Users
                          select item.UserName into item
                          orderby item
                          select item).ToList<string>();
            }
            return result;
        }

        [OperationContract, WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public string GetUserHistory(string id, string startTime, string endTime)
        {
            DateTime startDateTime;
            DateTime dateTime;
            if (string.IsNullOrEmpty(id) || !DateTime.TryParse(startTime, out startDateTime) || !DateTime.TryParse(endTime, out dateTime))
            {
                throw new Exception("missed parameter(-s)");
            }
            UserHistoryItem[] objectsToJson;
            using (RDDBContext rDDBContext = new RDDBContext())
            {
                objectsToJson = (from item in rDDBContext.t_WebSessions.ToList<t_WebSessions>()
                                 where item.c_WSUserName == id && item.c_WSTimeLogin >= startDateTime
                                 select item into userHistory
                                 select new UserHistoryItem
                                 {
                                     UserLogin = userHistory.c_WSUserName,
                                     StartVisitTime = userHistory.c_WSTimeLogin.ToLocalTime().ToString(this.timeFormat),
                                     EndVisitTime = (userHistory.c_WSTimeLogout.HasValue ? userHistory.c_WSTimeLogout.Value.ToLocalTime().ToString(this.timeFormat) : string.Empty)
                                 } into item
                                 orderby item.StartVisitTime descending
                                 select item).ToArray<UserHistoryItem>();
            }
            return base.ToJson(objectsToJson);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
