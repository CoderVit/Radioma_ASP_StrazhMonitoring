﻿using StrazhMonitoring.Infrastructure.Context;
using StrazhMonitoring.Instances.UserAdministration;
using StrazhMonitoring.Services.Base;
using System;
using System.Data.Entity.Core.Objects;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;

namespace StrazhMonitoring.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed), ServiceContract]
    public class UserObjectService : ServiceBase
    {
        [OperationContract, WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public string ObjectsGet(string userName)
        {
            if (string.IsNullOrEmpty(userName))
            {
                throw new ArgumentNullException("missed parameter(-s)");
            }
            var objectParameter = new ObjectParameter("OSAll", typeof(string));
            var objectParameter2 = new ObjectParameter("OSLinked", typeof(string));
            using (var rddbContext = new RDDBContext())
            {
                rddbContext.p_WUGPOStringsGet(this.sessionId, userName, objectParameter, objectParameter2, new ObjectParameter("OSWithAlertsAllowed", typeof(string)));
            }
            return ToJson(new ObjectsData(objectParameter.Value.ToString(), objectParameter2.Value.ToString()));
        }

        [OperationContract, WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public string AlertsGet(string userName)
        {
            if (string.IsNullOrEmpty(userName))
            {
                throw new ArgumentNullException("missed parameter(-s)");
            }
            var objectParameter = new ObjectParameter("OSLinked", typeof(string));
            var objectParameter2 = new ObjectParameter("OSWithAlertsAllowed", typeof(string));
            using (var rddbContext = new RDDBContext())
            {
                rddbContext.p_WUGPOStringsGet(this.sessionId, userName, new ObjectParameter("OSAll", typeof(string)), objectParameter, objectParameter2);
            }
            return ToJson(new ObjectsData(objectParameter.Value.ToString(), objectParameter2.Value.ToString()));
        }

        [OperationContract, WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public string ObjectsSet(string userName, string userData)
        {
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(userData))
            {
                throw new ArgumentNullException("missed parameter(-s)");
            }
            var objectParameter = new ObjectParameter("error_text", typeof(string));
            using (var rddbContext = new RDDBContext())
            {
                rddbContext.p_WUGPOStringsSet(sessionId, userName, userData, null, objectParameter);
            }
            string text = objectParameter.Value.ToString();
            return ToJson(string.IsNullOrEmpty(text) ? "OK" : text);
        }

        [OperationContract, WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public string AlertsSet(string userName, string userData)
        {
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(userData))
            {
                throw new ArgumentNullException("missed parameter(-s)");
            }
            var objectParameter = new ObjectParameter("error_text", typeof(string));
            using (var rddbContext = new RDDBContext())
            {
                rddbContext.p_WUGPOStringsSet(sessionId, userName, null, userData, objectParameter);
            }
            string text = objectParameter.Value.ToString();
            return ToJson(string.IsNullOrEmpty(text) ? "OK" : text);
        }
    }
}
