﻿using StrazhMonitoring.Infrastructure.Context;
using StrazhMonitoring.Instances.PersonalSettings;
using StrazhMonitoring.Services.Base;
using System;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;

namespace StrazhMonitoring.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed), ServiceContract]
    public class UserSettingService : ServiceBase, IDisposable
    {
        [OperationContract, WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public UserSettings GetSettings()
        {
            UserSettings userSettings = new UserSettings();
            using (var context = new RDDBContext())
            {
                context.p_WUSGet(this.sessionId, new int?(0)).ToList<p_WUSGet_Result>().ForEach(delegate(p_WUSGet_Result item)
                {
                    bool flag = item.c_WUSValue != "0";
                    switch (item.c_WUSId)
                    {
                        case 1:
                            userSettings.IsSoundEnabled = flag;
                            return;
                        case 2:
                            userSettings.IsObjectHistiryWrap = flag;
                            return;
                        case 3:
                            userSettings.IsHistoryBaloonShow = flag;
                            return;
                        case 4:
                            userSettings.IsUserAdmin = flag;
                            return;
                        case 5:
                            userSettings.IsAlertsEnabled = flag;
                            return;
                        default:
                            return;
                    }
                });
            }
            return userSettings;
        }

        [OperationContract, WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public void SetSettings(int settingId, bool settingValue)
        {
            string wUSValue = Convert.ToInt16(settingValue).ToString();
            using (RDDBContext rDDBContext = new RDDBContext())
            {
                rDDBContext.p_WUSSet(this.sessionId, new int?(settingId), wUSValue);
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
