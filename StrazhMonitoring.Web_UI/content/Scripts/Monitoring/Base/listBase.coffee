﻿# CoffeeScript
@monitoring = @monitoring || {}
@monitoring.base = @monitoring.base || {}

class @monitoring.base.listBase
    constructor: ->
        @init()

    init: ->
        self = @
        self.objs = ko.observableArray()
        self.errMsg = ko.observable('')

    clean: ->
        self = @
        self.errMsg('')
        self.objs([])
