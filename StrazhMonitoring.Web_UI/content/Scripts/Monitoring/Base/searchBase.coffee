﻿# CoffeeScript
@monitoring = @monitoring || {}
@monitoring.base = @monitoring.base || {}

class @monitoring.base.searchBase extends @monitoring.base.listBase
    constructor: (data) ->
        @init(data)

    init: (n)-> 
        self = @
        @servLnk = n.lnk
        @selectDt = ko.observableArray([])
        @stTm = ko.observable(n.stTm)
        @endTm = ko.observable(n.endTm)
        @objSel = ko.observable('')
        super
        ko.bindingHandlers.selectpicker = 
            init: (n) ->
                i = $(n)
                i.addClass("selectpicker").selectpicker()
                i.on "change", -> self.objSel(this.value)
            update: (n, t) ->
                r = ko.utils.unwrapObservable(t())
                i = $(n)
                i.selectpicker("val", r)
                i.selectpicker("refresh")

    getSelectData: (n) ->
        self = @
        showWaitCover()
        $.getJSON self.servLnk + "/" + n
        .done (response) ->
            self.selectDt(response.d)
            $(".selectpicker").selectpicker("refresh")
        .always ->
            hideWaitCover()

    getData: (n) ->
        self = @
        self.clean()
        $.getJSON self.servLnk + "/" + n, 
            id: self.objSel(),
            startTime: self.stTm(),
            endTime: self.endTm()
        .done (response) -> 
            data = JSON.parse(response.d)
            self.errMsg("История отсутствует")
            if data.length is 0
                self.errMsg("История отсутствует")
                return
            self.objs(data)
        .fail ->
            self.errMsg("Ошибка получения данных")
        .always -> 
            hideWaitCover()