﻿# CoffeeScript
@monitoring = @monitoring || {}
@monitoring.users = @monitoring.users || {}
@monitoring.users.administration = @monitoring.users.administration || {}

class @monitoring.users.administration.ObjItm
    constructor: (id, isSelect) ->
        self = @
        @id= id
        @isSelected = ko.observable(isSelect)
