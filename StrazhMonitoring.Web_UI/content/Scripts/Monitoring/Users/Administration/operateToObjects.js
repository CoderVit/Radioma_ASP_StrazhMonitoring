﻿(function () {
    'use strict';
    this.monitoring = this.monitoring || {};
    this.monitoring.users = this.monitoring.users || {};
    this.monitoring.users.administration = this.monitoring.users.administration || {};

    monitoring.users.administration.operateToObjects = function () {
        function n() {
            var n = this;
            n.titl = ko.observable();
            n.objs = ko.observableArray();
            n.selLgn = ko.observableArray();
            n.getTp = "";
            n.isAll = ko.observable(false);
            n.calcInpVal = ko.pureComputed({
                read: function () {
                    var r, t, i; return i = "", r = n.objs(),
                    t = null, ko.utils.arrayForEach(r, function (n, u) { var f, e, o; return n.isSelected() ? (f = n.id, o = f + 1, e = r[u + 1] && r[u + 1].isSelected() ? r[u + 1].id : null, o === e && t == null ? (i += f, t = f) : o === e && t != null ? true : o !== e && t != null ? (i += "-" + f + ",", t = null) : i += f + ",") : true }),
                    i.substring(0, i.length - 1);
                },
                write: function (n) { return console.log("write", n) },
                owner: n
            });
        }
        return n.prototype.showPanel = function (title, t, type, objData) {
            if (objData === null) return;
            document.querySelector("#usObjCovDiv").style.display = "block";
            const self = this;
            self.selLgn(t),
            self.getTp = type;
            self.titl(title),
            self.parseData(objData);
        },
        n.prototype.panelClean = function () {
            const self = this;
            $("#pnlCont").slideUp(function () {
                self.titl("");
                self.selLgn("");
                self.getTp = null;
                self.objs([]);
                document.querySelector("#usObjCovDiv").style.display = "none";
            });
        },
        n.prototype.tooggled = function (n) {
            var t; return t = this, ko.utils.arrayForEach(t.objs(),
            function (t) {
                var i;
                if (t === n) return i = t.isSelected() ? false : true, t.isSelected(i);
            });
        },
        n.prototype.objClass = function (n) {
            var t; return t = "objItm ", t += n.id.toString().length >= 4 ? "lrgObj" : "smlObj", n.isSelected() && (t += " objSel"),
            t;
        },
        n.prototype.parseData = function (n) {
            var e, i, o, r;
            const parsedObjs = n.objs.split(","),
                objects = [];
            for (let dataIterator = 0; dataIterator < parsedObjs.length; dataIterator++) {
                const obj = parsedObjs[dataIterator];
                if (obj.indexOf("-") !== -1) {
                    const splittedObj = obj.split("-");
                    for (e = i = o = parseInt(splittedObj[0]), r = parseInt(splittedObj[1]) + 1; o <= r ? i < r : i > r; e = o <= r ? ++i : --i) {
                        objects.push(e);
                    }
                } else {
                    if (obj !== "") {
                        objects.push(parseInt(obj));
                    }
                }
            }
            if (!objects.length) return;
            this.parseObjects(objects, n.binds);
        },
        n.prototype.parseObjects = function (objects, selectedObjs) {
            var e, r, h, u, c;
            var splittedObjs = selectedObjs.split(","),
                vmObjs = [];
            for (let objectIndex = 0; objectIndex < objects.length; objectIndex++) {
                const currentObj = splittedObjs[objectIndex];
                if (currentObj.indexOf("-") !== -1) {
                    for (c = currentObj.split("-"), e = r = h = parseInt(c[0]), u = parseInt(c[1]) + 1; h <= u ? r < u : r > u; e = h <= u ? ++r : --r) {
                        vmObjs.push(new monitoring.users.administration.ObjItm(e, selectedObjs.indexOf(e) !== -1));
                    }
                } else {
                    vmObjs.push(new monitoring.users.administration.ObjItm(parseInt(currentObj), selectedObjs.indexOf(parseInt(currentObj)) !== -1));
                }
            }
            self.objs(vmObjs);
            $("#pnlCont").slideDown();
            $("#pnlCont").focus();
        },
        n.prototype.keyHandl = function (n, t) {
            const i = t.keyCode;
            switch (i) {
                case 27:
                    this.panelClean();
                    break;
                default: throw Error('incorrect switch value');

            }
        },
        n.prototype.saveData = function () {
            const self = this;
            $.ajax({
                url: "/services/userObjectService.svc/" + self.getTp + "Set",
                type: "POST",
                dataType: "json",
                contentType: "application/json",
                data: JSON.stringify({
                    userName: self.selLgn(),
                    userData: self.isAll() ? 0 : self.calcInpVal()
                }),
                success: function (t) {
                    if (JSON.parse(t.d) === "OK") {
                        n.panelClean();
                    }
                },
                error: function () { return console.error("save data error occured") }
            });
        },
        n;
    }();
}).call(this);