﻿(function () {
    var n = function (n, i) {
        function u() { this.constructor = n }
        for (var r in i) t.call(i, r) && (n[r] = i[r]);
        return u.prototype = i.prototype, n.prototype = new u, n.__super__ = i.prototype, n;
    },
    t = {}.hasOwnProperty; this.monitoring = this.monitoring || {};
    this.monitoring.users = this.monitoring.users || {};
    this.monitoring.users.administration = this.monitoring.users.administration || {};
    monitoring.users.administration.usersManage = function (t) {
        function i() {
            var n; showWaitCover();
            n = this; n.newUser = {
                login: ko.observable(""),
                password: ko.observable("")
            };
            n.usrLnk = "/services/userAdministrationService.svc"; n.objsLnk = "/services/userObjectService.svc"; n.usrs = ko.observableArray([]);
            n.usrSel = ko.observable();
            n.objsMan = new monitoring.users.administration.operateToObjects; n.isnuPanShow = ko.observable(false);
            ko.bindingHandlers.nwUsrPnl = {
                update: function (n, t) {
                    var i; return i = ko.unwrap(t()),
                    i ? $(n).show("slow") : $(n).hide("slow");
                }
            };
            n.isChUsPassShow = ko.observable(false);
            ko.bindingHandlers.chUsrPassPnl = {
                update: function (n, t) {
                    var i, r, u; return u = ko.unwrap(t()),
                    i = u ? "block" : "none", r = $(n),
                    r.css("display", i),
                    r.find("#chPassCont").css("display", i);
                }
            };
            n.init();
        }
        return n(i, t),
        i.prototype.init = function () {
            return this.getUsers(),
            i.__super__.init.apply(this, arguments);
        },
        i.prototype.addUsr = function () {
            var t, n; return n = this, t = {
                userName: n.newUser.login(),
                password: n.newUser.password()
            },
            n.req(n.usrLnk + "/userAdd", t).done(function (i) {
                var r; return r = JSON.parse(i.d),
                r.code === 0 ? (n.clnNwUsr(),
                n.usrs.push({ userName: t.userName, isLockedOut: false }),
                n.usrs.sort(function (n, t) { return n.userName.toLowerCase() > t.userName.toLowerCase() ? 1 : -1 })) : n.errMsg(r.message);
            }).always(function () { return hideWaitCover() });
        },
        i.prototype.remUsr = function (n) {
            var i, t; return i = n.userName ? n.userName : n.usrSel(),
            t = this, t.req(t.usrLnk + "/userDelete", { userName: i }).done(function (n) {
                var r; return r = JSON.parse(n.d),
                r.code === 0 ? ko.utils.arrayFirst(t.usrs(),
                function (n) {
                    return n.userName !== i ? false : true;
                }) : t.errMsg(r.message);
            }).always(function () { return hideWaitCover() });
        },
        i.prototype.getUsers = function () {
            var n; return n = this, n.req(n.usrLnk + "/usersGet").done(function (t) {
                var i; return i = JSON.parse(t.d),
                i.length !== 0 && n.usrs(i),
                hideWaitCover();
            }).error(function () { return n.errMsg("ошибка получения данных") }).always(function () { return hideWaitCover() });
        },
        i.prototype.mnUnlkUsr = function () { },
        i.prototype.clnNwUsr = function () {
            var n; return $("#newUserForm").hide("slow"),
            n = this, n.isnuPanShow(false),
            n.errMsg(""),
            n.newUser.login(""),
            n.newUser.password("");
        },
        i.prototype.getObjs = function (n) {
            var i, t;
            return i = n.userName ? n.userName : n.usrSel(),
                t = this,
            t.req(t.objsLnk + "/objectsGet?userName=" + i)
                .done(function (n) {
                    var objs = JSON.parse(n.d);
                    return t.objsMan.showPanel("Объекты пользователя", i, "objects",
                    {
                        objs: objs.objectsCollection,
                        binds: objs.objectsBindinds
                    });
                })
                .always(function () { return hideWaitCover() });
        },
        i.prototype.getAlrt = function (n) {
            var i, t; return i = n.userName ? n.userName : n.usrSel(),
            t = this,
                t.req(t.objsLnk + "/alertsGet?userName=" + i)
                    .done(function (n) {
                        return t.objsMan.showPanel("Уведомления для объектов пользователя", i, "alerts", n.d);
                    }).always(function () {
                        hideWaitCover();
                    });
        },
        i.prototype.req = function (n, t) {
            var i; return showWaitCover(),
            i = this, $.ajax({ url: n, type: t == null ? "GET" : "POST", dataType: "json", contentType: "application/json", data: t != null ? JSON.stringify(t) : void 0 });
        },
        i;
    }(monitoring.base.listBase);
}).call(this);